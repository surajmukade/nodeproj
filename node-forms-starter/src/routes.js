const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
  res.render('index');
});

// routes.js
router.get('/contact', (req, res) => {
  res.render('contact');
});
router.get('/contact', (req, res) => {
  res.render('contact', {
    data: {},
    errors: {}
  });
});

router.post('/contact', (req, res) => {
  res.render('contact', {
    data: req.body, // { message, email }
    errors: {
      message: {
        msg: 'A message is required'
      },
      email: {
        msg: 'That email doesn‘t look right'
      }
    }
    
  });
 // req.flash('success', 'Thanks for the message! I‘ll be in touch :)');
  //res.redirect('/');
});
module.exports = router;

