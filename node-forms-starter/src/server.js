const path = require('path');
const express = require('express');
const layout = require('express-layout');

const routes = require('./routes');
const app = express();
// server.js
const bodyParser = require('body-parser');
//const flash = require('express-flash');



app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

const middlewares = [
  bodyParser.urlencoded({ extended: true }),
  layout(),
  express.static(path.join(__dirname, 'public')),
  //flash(),
];
app.use(middlewares);
app.post('/contact', function(req, res) {
  res.send('You sent the name "' + req.body.message + '".');
});

app.use('/', routes);

app.use((req, res, next) => {
  res.status(404).send("Sorry can't find that!");
});

app.use((err, req, res, next) => {
  console.error(err.stack);
  res.status(500).send('Something broke!');
});

app.listen(3000, () => {
  console.log('App running at http://localhost:3000');
});
